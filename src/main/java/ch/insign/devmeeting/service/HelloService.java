package ch.insign.devmeeting.service;

import org.springframework.web.bind.annotation.*;

@RestController
public class HelloService {

    @RequestMapping(method = RequestMethod.GET, path = "/", produces = "text/plain")
    public String helloWorld() {
        return "Hello World";
    }

    @RequestMapping(method = RequestMethod.GET, path = "/{name}", produces = "text/plain")
    public String hi(@PathVariable String name) {
        return "Hi " + name;
    }
}
